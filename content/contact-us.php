<?php

$error = array(
	'name' => false,
	'email' => false,
	'message' => false,
);

function formField($name, $label, $type) {
	global $error;

	if ($error[$name] != '') {
		echo '<div class="form-group has-error">';
	} else {
		echo '<div class="form-group">';
	}
    
	echo "<label for='$name' class='col-sm-3 control-label'>$label</label>";
	if ($type != 'textarea')
		echo "<div class='col-sm-9'><input type='$type' class='form-control' name='$name' required /></div>";
	else
		echo "<div class='col-sm-9'><textarea class='form-control' name='$name' required></textarea></div>";
	echo "</div>\n";
}

$hasError = false;
$emailSent = false;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	foreach (array_keys($error) as $f) {
		if ($_POST[$f] === "") {
			$error[$f] = true;
			$hasError = true;
		}
	}

	if (!$hasError) {
		mail("contact@precede.eu", "Precede demo request", print_r($_POST, true)); 
		$emailSent = true;
	}
}

?>

<h1>Contact Us</h1>
<?php if ($emailSent) { ?>
<div class="alert alert-success">
<strong>Our consultants have been notified</strong><br />

We'll contact you soon so you can try Precede and start your free trial.
</div>

<?php } else { ?>

<div class="span7">	
	<p>E-mail us directly at <a class="contact-email"></a> or use the form below.</p>
	
</div>

<div class="row">
   <div class="col-xs-8">
			<?php if ($hasError) { ?>
				<div class="alert alert-danger">
					All fields are required
				</div>
			<?php } ?>

      <form class="form-horizontal" role="form" method="post">
			<?php formField('name', 'Name', 'text'); ?>
			<?php formField('email', 'Email', 'email'); ?>
			<?php formField('message', 'Message', 'textarea'); ?>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary pull-right">Send</button>
          </div>
        </div>
      </form>
   </div>
</div>

<?php } ?>
