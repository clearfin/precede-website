import sys
import os
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
from generate import generate
import subprocess
import signal
import urllib

root = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

try:
  with_php = sys.argv[1] == '--with-php'
except IndexError:
  with_php = False

class StaticHandler(SimpleHTTPRequestHandler):
  def do_GET(self):
    os.chdir(root)

    if self.path.startswith('/assets'):
      os.chdir('assets')
      self.path = self.path.replace('/assets', '')
    elif self.path.endswith('.php'):
      self.path = "/php-disabled.html"
      generate()
      os.chdir('www')
    else:
      generate()
      os.chdir('www')
    SimpleHTTPRequestHandler.do_GET(self)

class PhpHandler(StaticHandler):
  def do_GET(self):
    os.chdir(root)

    if self.path.startswith('/assets'):
      StaticHandler.do_GET(self)
    else:
      generate()
      self.copyfile(urllib.urlopen('http://localhost:5401' + self.path), self.wfile)

  def do_POST(self):
    generate()
    length = int(self.headers.getheader('content-length'))
    self.copyfile(urllib.urlopen('http://localhost:5401' + self.path, self.rfile.read(length)), self.wfile)

ServerClass  = BaseHTTPServer.HTTPServer
port = 5400
server_address = ('127.0.0.1', port)

if with_php:
  subprocess.Popen(['php', '-S', 'localhost:5401', '-t', 'www'])
  httpd = ServerClass(server_address, PhpHandler)
else:
  httpd = ServerClass(server_address, StaticHandler)

sa = httpd.socket.getsockname()
print "Serving HTTP on", sa[0], "port", sa[1], "..."
httpd.serve_forever()
