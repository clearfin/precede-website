#!/usr/bin/env python

import os
import markdown2
import codecs

def convert(data):
  return markdown2.markdown(data,
      extras=[
        'cuddled-lists', 
        'wiki-tables',
      ])

def utf8open(f, mode='r'):
  return codecs.open(f, mode, 'utf-8')

def generate():
  if not os.path.exists('www'):
    os.makedirs('www')

  sitemap = set()

  for dirname,_,files in os.walk('content'):
    template = utf8open(os.path.join(dirname, 'template.html')).read()

    for f in files:
      if f == 'template.html':
        continue

      if f.startswith('.'):
        continue

      infile = os.path.join(dirname, f)

      extension = '.html'
      if f.endswith('.php'):
        extension = '.php'
      outfile = os.path.join(dirname.replace('content', 'www'), f.split('.')[0] + extension)

      content = utf8open(infile).read()
      if f.endswith('.mdown'):
        content = convert(content)

      if f.endswith('.raw.html') or f.endswith('.raw.php'):
        utf8open(outfile, 'w').write(content)
      else:
        utf8open(outfile, 'w').write(template.replace('## CONTENT MARKER ##', content))
      sitemap.add(outfile)

  f = utf8open('www/sitemap.xml', 'w')
  f.write('<?xml version="1.0" encoding="UTF-8"?>\n')
  f.write('<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n')
  for p in sitemap:
    f.write("\t<url><loc>http://www.precede.eu/%s</loc></url>\n" % p.replace('www/', ''))
  f.write('</urlset>\n')
  f.close()

if __name__ == '__main__':
  generate()
